package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeUint()
	typeUint()
	typeInt()
	typeFloat()
}

func typeFloat() {
	fmt.Println("===START type Float===")
	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25

	uintNumber += 1 << 21
	uintNumber += 1 << 31
	var floatNumber float32
	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)

	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("пример ошибки 1:", c)

	a = 99999845
	b2 := float32(a)
	fmt.Println("пример ошибки 2:", b2)

	a4 := 5.2
	b4 := 4.1
	fmt.Println(a4 + b4)
	fmt.Println(a4+b4 == 9.3)

	c4 := 5.2
	d4 := 2.1

	fmt.Println(c4 + d4)
	fmt.Println(c4+d4 == 7.3)

	fmt.Println("===END type Float===")
}

func typeUint() {
	fmt.Println("===START type Uint===")
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint 8 max value", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")

	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint 16 max vtalue", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint 32 max value", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint 64 max value", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("===END type Uint===")
}

func typeInt() {
	var uintNumber uint8 = 1 << 7
	var from = int8(uintNumber)
	uintNumber--
	var to = int8(uintNumber)
	fmt.Println("int8 min value", from, "int8 max value", to)

	var uintNumber1 uint16 = 1 << 15
	var from1 = int16(uintNumber1)
	uintNumber1--
	var to1 = int16(uintNumber1)
	fmt.Println("int16 min value", from1, "int16 max value", to1)

	var uintNumber2 uint32 = 1 << 31
	var from2 = int32(uintNumber2)
	uintNumber2--
	var to2 = int32(uintNumber2)
	fmt.Println("int32 min value", from2, "int32 max value", to2)

	var uintNumber3 uint64 = 1 << 63
	var from3 = int64(uintNumber3)
	uintNumber3--
	var to3 = int64(uintNumber3)
	fmt.Println("int64 min value", from3, "int64 max value", to3)
}
