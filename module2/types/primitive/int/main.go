package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")

	typeInt()
	typeUint()
}

func typeInt() {
	var uintNumber uint8 = 1 << 7
	var from = int8(uintNumber)
	uintNumber--
	var to = int8(uintNumber)
	fmt.Println("int8 min value", from, "int8 max value", to)

	var uintNumber1 uint16 = 1 << 15
	var from1 = int16(uintNumber1)
	uintNumber1--
	var to1 = int16(uintNumber1)
	fmt.Println("int16 min value", from1, "int16 max value", to1)

	var uintNumber2 uint32 = 1 << 31
	var from2 = int32(uintNumber2)
	uintNumber2--
	var to2 = int32(uintNumber2)
	fmt.Println("int32 min value", from2, "int32 max value", to2)

	var uintNumber3 uint64 = 1 << 63
	var from3 = int64(uintNumber3)
	uintNumber3--
	var to3 = int64(uintNumber3)
	fmt.Println("int64 min value", from3, "int64 max value", to3)

}

func typeUint() {

	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint 8 max value", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")

	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint 16 max value", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint 32 max value", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint 64 max value", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")

}
