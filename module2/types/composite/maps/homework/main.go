package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{Name: "https://github.com/docker/compose1",
			Stars: 25600},
		{
			Name:  "https://github.com/docker/compose2",
			Stars: 28000},
		{Name: "https://github.com/docker/compose3",
			Stars: 23400},
		{
			Name:  "https://github.com/docker/compose6",
			Stars: 675849},
		{
			Name:  "https://github.com/docker/compose7",
			Stars: 685849},
		{
			Name:  "https://github.com/docker/compose8",
			Stars: 11111},
		{
			Name:  "https://github.com/docker/compose9",
			Stars: 11112},
		{
			Name:  "https://github.com/docker/compose10",
			Stars: 46789},
		{
			Name:  "https://github.com/docker/compose11",
			Stars: 39485},
		{
			Name:  "https://github.com/docker/compose12",
			Stars: 22222},
		{
			Name:  "https://github.com/docker/compose13",
			Stars: 84637},
		{
			Name:  "https://github.com/docker/compose14",
			Stars: 675800},

		// сюда впишите ваши остальные 12 структур
	}
	f := make(map[string]int, len(projects))

	for h := 0; h < len(projects); h++ {

		f[projects[h].Name] = projects[h].Stars

	}

	for t := range f {
		fmt.Println(f[t])
	}

	// в цикле запишите в map
	for h := 0; h < len(projects); h++ {

		f[projects[h].Name] = projects[h].Stars

	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for t := range f {
		fmt.Println(f[t])
	}
}
